package u.urizen.project.ui;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import u.urizen.project.UrizenDirectories;
import u.urizen.project.UProjectSetupProcess;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.JSeparator;
import javax.swing.JCheckBox;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ProjectSetupPanel extends JPanel
{
	private UProjectSetupProcess setup;

	private JTextField fldProjectName;
	private JTextField fldLocation;
	private JLabel lblMainClassName;
	private JLabel lblLoadScreenName;
	private JLabel lblGameModeName;

	private JFileChooser fileChooser;
	private JCheckBox chckbxIncludeUrizenJar;
	
	/**
	 * Create the panel.
	 */
	public ProjectSetupPanel()
	{
		setup = new UProjectSetupProcess();
		
		makeComponents();
		
	}
	
	private void browseLocations()
	{
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.showDialog(this, "Select");
		
		modelChanged();
	}
	
	private void modelChanged()
	{
		setup.name = fldProjectName.getText();
		
		setup.includeUrizenJar = chckbxIncludeUrizenJar.isSelected();
		
		lblMainClassName.setText(setup.getMainClassName());
		lblLoadScreenName.setText(setup.getLoadScreenName());
		lblGameModeName.setText(setup.getInitialModeName());
		
		File selected = fileChooser.getSelectedFile();
		
		if(selected != null)
		{
			fldLocation.setText(selected.getPath());
		}
		
		fileChooser.setSelectedFile(null);
	}
	
	private void generate()
	{
		setup.outputLocation = fldLocation.getText();
		
		String err = setup.ensureProjectValid();
		
		if(err != null)
		{
			JOptionPane.showMessageDialog(this, err, "Invalid setup", JOptionPane.ERROR_MESSAGE);
			
			return;
		}
		
		
	}
	
	private void makeComponents()
	{
		fileChooser = new JFileChooser();
		
		JLabel lblProjectName = new JLabel("Project Name");
		
		fldProjectName = new JTextField();
		fldProjectName.setColumns(10);
		
		fldProjectName.setText("New Project");
		
		JLabel lblLocation = new JLabel("Location");
		
		fldLocation = new JTextField();
		fldLocation.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				browseLocations();
			}
		});
		fldLocation.setEditable(false);
		fldLocation.setColumns(10);
		
		JButton btnBrowse = new JButton("Browse...");
		btnBrowse.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						browseLocations();
					}
				});
		
		JSeparator separator = new JSeparator();
		
		JLabel lblMainClass = new JLabel("Main Class:");
		
		JLabel lblLoadScreen = new JLabel("Load Screen:");
		
		lblMainClassName = new JLabel("New label");
		
		lblLoadScreenName = new JLabel("New label");
		
		JLabel lblGameMode = new JLabel("Game Mode:");
		
		lblGameModeName = new JLabel("New label");
		
		JPanel panel = new JPanel();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(separator, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblProjectName)
								.addComponent(lblLocation))
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(fldLocation, GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnBrowse))
								.addComponent(fldProjectName, GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(lblMainClass, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(lblLoadScreen, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblMainClassName)
										.addComponent(lblLoadScreenName)))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblGameMode, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(lblGameModeName)))
							.addGap(63)
							.addComponent(panel, GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblProjectName)
						.addComponent(fldProjectName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLocation)
						.addComponent(fldLocation, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnBrowse))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblMainClass)
								.addComponent(lblMainClassName))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblLoadScreen)
								.addComponent(lblLoadScreenName))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblGameModeName)
								.addComponent(lblGameMode)))
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap(148, Short.MAX_VALUE))
		);
		
		JButton btnGenerateProject = new JButton("Generate Project");
		
		chckbxIncludeUrizenJar = new JCheckBox("Include urizen jar");
		chckbxIncludeUrizenJar.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				modelChanged();
			}
		});
		chckbxIncludeUrizenJar.setSelected(true);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(109, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(chckbxIncludeUrizenJar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnGenerateProject, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addComponent(chckbxIncludeUrizenJar)
					.addPreferredGap(ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
					.addComponent(btnGenerateProject))
		);
		panel.setLayout(gl_panel);
		btnGenerateProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generate();
			}
		});
		setLayout(groupLayout);
		
		fldProjectName.getDocument().addDocumentListener(new DocumentListener()
		{

			@Override
			public void insertUpdate(DocumentEvent e)
			{
				modelChanged();
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				modelChanged();
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				modelChanged();
			}
	
		});
		
		modelChanged();
	}
}
