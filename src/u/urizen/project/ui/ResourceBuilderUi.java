package u.urizen.project.ui;

import java.awt.EventQueue;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URISyntaxException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JTextArea;

import u.U;
import u.urizen.project.UResourceGeneratorProcess;
import u.urizen.project.UPM;
import u.urizen.project.UPM.OutListener;

import java.awt.Window.Type;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ResourceBuilderUi
{

	private JFrame frmResourceGenerator;

	public Console console;
	private JPanel panel;
	private JButton btnRegen;
	
	private void generate()
	{
		new Thread(new Runnable()
				{
			
				@Override
				public void run()
				{
					btnRegen.setEnabled(false);
					
						File runLocation = null;
						
						btnRegen.setEnabled(false);
						
						try
						{
							console.appendln("\nGenerating...");
							
							runLocation = new File(ResourceBuilderUi.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
							
							UResourceGeneratorProcess gen = new UResourceGeneratorProcess();
							gen.projectRoot = runLocation;
						
							//gen.generate();

						}
						catch (Exception e)
						{
							e.printStackTrace();
							
							UPM.err("\n" + U.getStackTraceAsString(e));
							UPM.err("Resource generation failed :(");
						}
						
						btnRegen.setEnabled(true);
						btnRegen.requestFocusInWindow();
					}
				}).start();
				


	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					
					ResourceBuilderUi window = new ResourceBuilderUi();
					window.frmResourceGenerator.setVisible(true);
					
					UPM.out("---Urizen Resource Generator---");
					
					window.btnRegen.requestFocusInWindow();
					
//					File runLocation = new File(ResourceBuilderUi.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
//										
//					window.console.onOut(runLocation.getPath(), false);
//					
//					ResourceGenerator gen = new ResourceGenerator();
//					gen.projectRoot = runLocation;
//					
//					gen.generate();
//					window.btnClose.requestFocusInWindow();
				
				}
				catch (Exception e)
				{
					e.printStackTrace();
					
					UPM.err("Swing exception : \n" + U.getStackTraceAsString(e));
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ResourceBuilderUi()
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frmResourceGenerator = new JFrame();
		frmResourceGenerator.setType(Type.UTILITY);
		frmResourceGenerator.setTitle("Resource Generator");
		frmResourceGenerator.setBounds(100, 100, 836, 463);
		frmResourceGenerator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JScrollPane scrollPane = new JScrollPane();
		frmResourceGenerator.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		console = new Console();
		console.setFont(new Font("Monospaced", Font.PLAIN, 15));
		scrollPane.setViewportView(console);
		
		UPM.listeners.add(new OutListener()
				{
					@Override
					public void onOut(String msg, boolean isError)
					{
						if(isError)
						{
							console.color(Color.RED).bold();
						}
						
						console.appendln(msg);
					}
				});
		
		panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		frmResourceGenerator.getContentPane().add(panel, BorderLayout.SOUTH);
		
		btnRegen = new JButton("Regenerate");
		btnRegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generate();
			}
		});
		panel.add(btnRegen);
	}

}
