package u.urizen.project.ui;

import u.urizen.project.UPM;

import javax.swing.JTextArea;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import java.awt.Color;
import java.awt.Font;

public class ConsoleOld extends JTextArea implements UPM.OutListener
{
	public ConsoleOld() {
		setFont(new Font("Monospaced", Font.PLAIN, 13));
		setForeground(Color.WHITE);
		setBackground(Color.DARK_GRAY);
		setEditable(false);
		
		DefaultCaret caret = (DefaultCaret) getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Override
	public void onOut(String msg, boolean isError)
	{
		Document doc = getDocument();
		
		Color c = isError ? Color.RED : Color.WHITE;
		
		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet att = sc.addAttribute(SimpleAttributeSet.EMPTY,
				StyleConstants.Foreground, c);
		
		att = sc.addAttribute(att, StyleConstants.Alignment, StyleConstants.ALIGN_CENTER);
		
		try
		{
			doc.insertString(doc.getLength(), msg + "\n", att);
			
			setCaretPosition(doc.getLength());
		}
		catch (BadLocationException e)
		{
			e.printStackTrace();
		}
	}

}
