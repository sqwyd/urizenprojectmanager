package u.urizen.project.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

import u.urizen.project.UPM;
import u.urizen.project.UPM.OutListener;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Window.Type;

public class UrizenProjectManagerUi
{

	private JFrame frmUrizenProjectSetup;
	Console console;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					
					UrizenProjectManagerUi window = new UrizenProjectManagerUi();
					window.frmUrizenProjectSetup.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UrizenProjectManagerUi()
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frmUrizenProjectSetup = new JFrame();
		frmUrizenProjectSetup.setResizable(false);
		frmUrizenProjectSetup.setTitle("Urizen Project Setup");
		frmUrizenProjectSetup.setBounds(100, 100, 614, 413);
		frmUrizenProjectSetup.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ProjectSetupPanel projectSetupPanel = new ProjectSetupPanel();
		
		JScrollPane scrollPane = new JScrollPane();
		
		GroupLayout groupLayout = new GroupLayout(frmUrizenProjectSetup.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addComponent(projectSetupPanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 598, Short.MAX_VALUE)
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 598, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(projectSetupPanel, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE))
		);
		
		console = new Console();
		scrollPane.setViewportView(console);
		frmUrizenProjectSetup.getContentPane().setLayout(groupLayout);
		
		UPM.listeners.add(new OutListener()
				{
					@Override
					public void onOut(String msg, boolean isError)
					{
						if(isError)
						{
							console.color(Color.RED).bold();
						}
						
						console.appendln(msg);
					}
				});
	}
}
