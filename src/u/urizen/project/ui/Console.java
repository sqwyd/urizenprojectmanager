package u.urizen.project.ui;

import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import java.awt.Font;

public class Console extends JScrollPane
{
	private final JTextPane textPane;
	
	private Color defaultColor = Color.white;
	private Color color;
	
	private boolean bold;
	
	public Console()
	{
		textPane = new JTextPane();
		textPane.setFont(new Font("Monospaced", Font.PLAIN, 14));
		textPane.setBackground(Color.DARK_GRAY);
		textPane.setEditable(false);
		
		DefaultCaret caret = (DefaultCaret) textPane.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		setViewportView(textPane);
		
		color(defaultColor);
	}

	public Console color(Color color)
	{
		this.color = color;
		
		return this;
	}
	
	public Console bold()
	{
		bold = true;
		
		return this;
	}
	
	public void resetStyle()
	{
		color = defaultColor;
		bold = false;
	}
	
	public void append(String str)
	{
		Document doc = textPane.getDocument();
		
		StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, color);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);
        aset = sc.addAttribute(aset, StyleConstants.Bold, bold);
		
		try
		{
			textPane.getDocument().insertString(doc.getLength(), str, aset);
		}
		catch (BadLocationException e)
		{
			e.printStackTrace();
		}
		
		resetStyle();
	}
	
	public void appendln(String str)
	{
		append(str + "\n");
	}
	
	public void append(Object obj)
	{
		append(obj.toString());
	}
	
	public void appendln(Object obj)
	{
		appendln(obj.toString());
	}
	
	public void clear()
	{
		textPane.setText("");
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2612334231050680421L;
}
