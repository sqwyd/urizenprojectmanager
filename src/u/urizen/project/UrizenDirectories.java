package u.urizen.project;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class UrizenDirectories
{		
	/* Resources */
		
	public static final String RESOURCE_ROOT = 				"/resources/";
	public static final String RESOURCE_ATLAS_SOURCES = 	RESOURCE_ROOT + "atlas/";
	public static final String RESOURCE_IMAGES = 			RESOURCE_ROOT + "img/";
	public static final String RESOURCE_SOUND = 			RESOURCE_ROOT + "sng/";
	public static final String RESOURCE_GENERATED = 		RESOURCE_ROOT + "generated/";
	public static final String RESOURCE_GENERATED_ATLAS = 	RESOURCE_GENERATED + "atlas/";
	public static final String RESOURCE_GENERATED_DEBUG = 	RESOURCE_GENERATED + "debug/";
	public static final String RESOURCE_GENERATED_IMG = 	RESOURCE_GENERATED + "img/";
	public static final String RESOURCE_GENERATED_INIT = 	RESOURCE_GENERATED + "init/";
	
	/* Source */
		
	public static final String SRC_ROOT = 		"/src/" + "%PACKAGE%/";
	public static final String SRC_COMPONENTS =	SRC_ROOT + "components/";
	public static final String SRC_ENTITIES =	SRC_ROOT + "entities/";
	public static final String SRC_LAUNCHER =	SRC_ROOT + "launcher/";
	public static final String SRC_MODES =		SRC_ROOT + "modes/";
	public static final String SRC_SCREENS =	SRC_ROOT + "screens/";
	public static final String SRC_SYSTEMS =	SRC_ROOT + "systems/";
	
	/* Lib */
	public static final String LIB_ROOT = "/libs/";
	
	public static final String[] PROJECT_DIRECTORIES = new String[]
			{
				RESOURCE_ROOT,
				RESOURCE_ATLAS_SOURCES,
				RESOURCE_IMAGES,
				RESOURCE_IMAGES,
				RESOURCE_SOUND,
				RESOURCE_GENERATED,
				RESOURCE_GENERATED_ATLAS,
				RESOURCE_GENERATED_DEBUG,
				RESOURCE_GENERATED_IMG,
				RESOURCE_GENERATED_INIT,
				SRC_ROOT,
				SRC_COMPONENTS,
				SRC_ENTITIES,
				SRC_LAUNCHER,
				SRC_MODES,
				SRC_SCREENS,
				SRC_SYSTEMS,
				LIB_ROOT
			};
	
	private UrizenDirectories() {}
}
