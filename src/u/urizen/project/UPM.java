package u.urizen.project;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class UPM
{	
	public static interface OutListener
	{
		void onOut(String msg, boolean isError);
	}
	
	public static final String URIZEN_VERSION = "0.0";
	
	public static final List<OutListener> listeners = new ArrayList<OutListener>();
	
	public static InputStream getResourceAsStream(String path)
	{
		path = "resources/" + path;
		
		InputStream in = UPM.class.getResourceAsStream(path);
		
		if(in == null)
		{
			fail("Failed to open InputStream for " + path);
		}
		
		return in;
	}
	
	
	public static void out(String msg)
	{
		System.out.println(msg);
		
		for(OutListener l : listeners)
		{
			l.onOut(msg, false);
		}
	}
	
	public static void err(String msg)
	{
		System.err.println(msg);
		
		for(OutListener l : listeners)
		{
			l.onOut(msg, true);
		}
	}
	
	public static void fail(String msg)
	{
		err(msg);
		throw new RuntimeException(msg);
	}
	
	private UPM() {}
}
