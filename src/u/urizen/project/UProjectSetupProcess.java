package u.urizen.project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import u.app.process.UProcess;
import u.urizen.Urizen;


public class UProjectSetupProcess extends UProcess
{
	public static final String KEY_PROJECT_NAME = "%PROJECT NAME%";
	public static final String KEY_PACKAGE_NAME = "%PACKAGE%";
	public static final String KEY_MAIN_CLASS = "%MAIN CLASS%";
	public static final String KEY_LOAD_SCREEN = "%LOAD SCREEN%";
	public static final String KEY_INITIAL_MODE = "%INITIAL MODE%";
	public static final String KEY_URIZEN_VERSION_MAJOR = "%URIZEN VERSION MINOR%";
	public static final String KEY_URIZEN_VERSION_MINOR = "%URIZEN VERSION MAJOR%";
	
	public String name;
	public boolean includeUrizenJar = true;
	
	public String outputLocation;
	
	private File outputDir;
	
	private final Map<String, String> templateValues;
	
	public UProjectSetupProcess()
	{
		super("Generating project");
		
		templateValues = new HashMap<String, String>();
	}
	
	@Override
	public void doProcess()
	{
		/* Ensure valid project */
		
		String error = ensureProjectValid();
		
		if(error != null)
		{
			throw new RuntimeException(error);
		}
		
		/* Begin */
		
		outputDir = new File(outputLocation);
		
		out("Setting up project \""	+ name + "\" in " + outputDir.getPath());
		
		makeTemplate();
		makeDirectories();
		copyFiles();
		
		out("Setup complete!");
	}
	
	private void makeTemplate()
	{
		templateValues.put(KEY_PROJECT_NAME, name);
		templateValues.put(KEY_PACKAGE_NAME, getPackageName());
		templateValues.put(KEY_MAIN_CLASS, getMainClassName());
		templateValues.put(KEY_LOAD_SCREEN, getLoadScreenName());
		templateValues.put(KEY_INITIAL_MODE, getInitialModeName());
		templateValues.put(KEY_URIZEN_VERSION_MAJOR, String.valueOf(Urizen.VERSION_MAJOR));
		templateValues.put(KEY_URIZEN_VERSION_MAJOR, String.valueOf(Urizen.VERSION_MINOR));
		templateValues.put(KEY_INITIAL_MODE, getInitialModeName());
		
		out("Generating template values.");
		
		for(String key : templateValues.keySet())
		{
			out(" - " + key + " = " + templateValues.get(key));
		}
	}
	
	private void makeDirectories()
	{
		out("Generating directories");
		
		for(String str : UrizenDirectories.PROJECT_DIRECTORIES)
		{
			File dir = new File(outputDir.getPath() + "/" + processTemplateString(str));
			dir.mkdirs();
			
			out(" - " + dir.getPath());
		}
	}
	
	private void copyFiles()
	{
		out("Copying files");
		
		/* Gradle */
		copyTemplateFile("setup/build.gradle", "build.gradle");
		copyFile("setup/build.bat", "build.bat");
		
		/* Init */
		copyFile("setup/init", UrizenDirectories.RESOURCE_GENERATED_INIT + "init");
		copyFile("setup/renderer", UrizenDirectories.RESOURCE_GENERATED_INIT + "renderer");
		
		/* Project */
		copyTemplateFile("setup/projectFile", "%PROJECT NAME%.urizen");
		
		/* Source */
		copyTemplateFile("setup/MainClass", UrizenDirectories.SRC_ROOT + "%MAIN CLASS%" + ".java");
		copyTemplateFile("setup/LoadScreen", UrizenDirectories.SRC_ROOT + "screens/%LOAD SCREEN%" + ".java");
		copyTemplateFile("setup/GameMode", UrizenDirectories.SRC_ROOT + "modes/%INITIAL MODE%" + ".java");
		
		/* Jar */
		if(includeUrizenJar)
		{
			String jarName = "urizen-" + UPM.URIZEN_VERSION + ".jar";
			copyFile("setup/" + jarName, UrizenDirectories.LIB_ROOT + jarName);
		}
		
		copyFile("setup/resgen.jar", "resgen.jar");
		
		/* Debug */
		copyFile("setup/debugFont.fnt", UrizenDirectories.RESOURCE_GENERATED_DEBUG + "debugFont.fnt");
		copyFile("setup/debugFont.png", UrizenDirectories.RESOURCE_GENERATED_DEBUG + "debugFont.png");
		copyFile("setup/pixel.png", UrizenDirectories.RESOURCE_GENERATED_IMG + "pixel.png");
	}
	
//	private void makeResources()
//	{
//		UResourceGeneratorProcess resGen = new UResourceGeneratorProcess();
//		
//		resGen.projectRoot = outputDir;
//		
//		resGen.generate();
//	}
	
	public String ensureProjectValid()
	{
		if(name == null)
		{
			return "Name is null!";
		}
		
		if(outputLocation == null)
		{
			return "Output location is null!";
		}
		
		if(name == null)
		{
			return "Project name not set.";
		}
		
		return null;
	}
	
	private void copyFile(String input, String output)
	{		
		out(" - Copy: " + input + " -> " + output);
		
		output = outputDir.getPath() + "/" + output;
		output = processTemplateString(output);
		
		
		
		InputStream in = UPM.getResourceAsStream(input);
				
		if(in == null)
		{
			fail("Failed to open InputStream for " + input);
		}
		
		File outFile = new File(output);

		try
		{
			Files.copy(in, outFile.toPath(), new CopyOption[] { StandardCopyOption.REPLACE_EXISTING });
		}
		catch (IOException e)
		{
			fail("Failed to copy " + input);
		}
	}
	
	private void copyTemplateFile(String input, String output)
	{
		out(" - Copy: " + input + " -> " + output);
		
		output = outputDir.getPath() + "/" + output;
		
		output = processTemplateString(output);
					
		InputStream in = UPM.getResourceAsStream(input);
		
		if(in == null)
		{
			fail("Could not find file: " + input);
		}
		
		Scanner scanner = new Scanner(in).useDelimiter("\\A");
		
		String str = scanner.next();
		scanner.close();
		
		str = processTemplateString(str);
		
		File outFile = new File(output);
		
		PrintWriter out = null;
		
		try
		{
			out = new PrintWriter(outFile);
			out.print(str);
		}		
		catch (FileNotFoundException e)
		{
			fail("Failed to open output file: " + output);
			throw new RuntimeException(e);
		}
		finally
		{
			if(out != null)
			{
				out.close();
			}
		}
	}	
	
	private String processTemplateString(String str)
	{
		for(String key : templateValues.keySet())
		{
			String value = templateValues.get(key);
			
			str = str.replaceAll(key, value);
		}
		
		return str;
	}
	
	public String getPackageName()
	{
		return name.replace(" ", "").toLowerCase().replaceAll("[^a-z]", "");
	}
	
	public String getMainClassName()
	{
		return name.replace(" ", "").replaceAll("[^a-zA-Z0-9]", "");
	}
	
	public String getLoadScreenName()
	{
		return getMainClassName() + "LoadScreen";
	}
	
	public String getInitialModeName()
	{
		return getMainClassName() + "GameMode";
	}
}
