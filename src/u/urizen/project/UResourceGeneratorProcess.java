package u.urizen.project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;

import u.app.process.UProcess;
import u.urizen.UrizenInitialiser;

public class UResourceGeneratorProcess extends UProcess
{
	private static final String[] ignoredResourceDirectories = new String[]
			{
					"\\generated", "\\atlas"
			};
	
	public File projectRoot;
	
	private File resourceDirectory;
	private File sourceDirectory;
	
	private final List<File> allFiles;
	
	private final List<String> entities;
	private final List<String> components;
	private final List<String> systems;
	private final List<String> resources;
	
	public UResourceGeneratorProcess()
	{
		super("Generating resources");
		
		allFiles = new ArrayList<File>();
		
		entities = new ArrayList<String>();
		components = new ArrayList<String>();
		systems = new ArrayList<String>();
		resources = new ArrayList<String>();
	}
	
	@Override
	public void doProcess()
	{
		if(projectRoot == null)
		{
			fail("No project location set!");
		}
		
		out("Genereting project resources");
		
		entities.clear();
		components.clear();
		systems.clear();
		
		out("Discovering resources.");
		
		findDirectories(projectRoot);
		makeClassNameLists(sourceDirectory);
		makeResourceList(resourceDirectory);
		makeAndSaveInitFile();
		
		makeTextureAtlas();
		
		out("Complete!");
	}
	
	private void findDirectories(File file)
	{
		if(file == null)
		{
			fail("Input file does not exist!");
		}
		
		if(file.isDirectory())
		{
			if(file.getName().endsWith("src"))
			{
				sourceDirectory = file;
				return;
			}
			
			if(file.getName().endsWith("resources"))
			{
				resourceDirectory = file;
				return;
			}
			

			File[] children = file.listFiles();
			
			if(children == null)
			{
				out("Access denied " + file);
				return;
			}
			
			for(File child : children)
			{
				findDirectories(child);
			}
		}
	}
	
	private void makeClassNameLists(File file)
	{
		if(file == null)
		{
			fail("Input file does not exist!");
		}
		
		if(file.isDirectory())
		{
			for(File child : file.listFiles())
			{
				makeClassNameLists(child);
			}
		}
		else if(file.getPath().endsWith(".java"))
		{
			String name = "";
			
			File parent = file.getParentFile();
			
			while(!parent.equals(sourceDirectory))
			{
				name = parent.getName() + "." + name;
				parent = parent.getParentFile();
			}
			
			name += file.getName()
					.replace(".java", "\"");
			
			name = "\"" + name;
			
			if(file.getPath().contains("\\entities\\"))
			{
				entities.add(name);		
				out(" - E: " + name);
			}
			else if(file.getPath().contains("\\systems\\"))
			{
				systems.add(name);		
				out(" - S: " + name);
			}
			else if(file.getPath().contains("\\components\\"))
			{
				components.add(name);		
				out(" - C: " + name);
			}
		}
	}
	
	private void makeResourceList(File file)
	{		
		if(file.isDirectory())
		{
			for(String ignore : ignoredResourceDirectories)
			{
				if(file.getPath().endsWith(ignore))
				{
					return;
				}
			}
			
			for(File child : file.listFiles())
			{
				makeResourceList(child);
			}
		}
		else
		{
			String name = "\"" + file.getPath()
					.replace(resourceDirectory.getPath(), "")
					.replace("\\", "/") + "\"";
			
			resources.add(name);
			out(" - R: " + name);
		}
	}
	
	private void makeAndSaveInitFile()
	{
		out("Creating init file.");
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("{ ")
			.append(UrizenInitialiser.KEY_SYSTEMS)
			.append(":[");
		
		for(String sysName : systems)
		{
			sb.append(sysName)
			.append(", ");
		}
				
		sb.append("], ")
			.append(UrizenInitialiser.KEY_ENTITIES)
			.append(":[");
		
		for(String entName : entities)
		{
			sb.append(entName)
			.append(", ");
		}

		sb.append("] ,")
		.append(UrizenInitialiser.KEY_COMPONENTS)
		.append(":[");
		
		for(String compName : components)
		{
			sb.append(compName)
			.append(", ");
		}
		
		sb.append("] ,")
		.append(UrizenInitialiser.KEY_RESOURCES)
		.append(":[");
		
		for(String resName : resources)
		{
			sb.append(resName)
			.append(", ");
		}
	
		sb.append("] }");
		
		String json = sb.toString();
				
		File initFile = new File(resourceDirectory.getPath() + "/generated/init/init");
		
		if(initFile.exists()) { initFile.delete(); }
		
		try
		{
			initFile.createNewFile();
		}
		catch (IOException e1)
		{
			fail("Failed to create init file.");
		}
		
		PrintWriter out = null;
		
		try
		{
			out = new PrintWriter(initFile);
			out.print(json);
		}
		catch (FileNotFoundException e)
		{
			fail("Failed to create init file PrintWriter.");
		}
		finally
		{
			if(out != null)
			{
				out.close();
			}
		}
	}
	
	private void makeTextureAtlas()
	{
		String input = (resourceDirectory.getPath() + "/atlas").replace("\\", "/");
		String output = (resourceDirectory.getPath() + "/generated/atlas").replace("\\", "/");;
		
		out("Generating TextureAtlas.");
		
		TexturePacker.process(resourceDirectory.getPath() + "/atlas",
				resourceDirectory.getPath() + "/generated/atlas",
				"textureatlas");
	}
	
//	private void fail(String arg)
//	{
//		UPM.err(arg);
//		
//		throw new RuntimeException(arg);
//	}
	
	public static void main(String[] args)
	{
		UResourceGeneratorProcess gen = new UResourceGeneratorProcess();
		gen.projectRoot = new File("C:/Users/Java/Desktop/Cabbages");
		
		//gen.generate();
	}
}
